# -*- coding: UTF-8 -*-

# This file is a part of teachmedijkstra which is a Python3 package designed
# for educational purposes to demonstrate the Dijkstra's algorithm.
#
# Copyright (C) 2021 Milan Petrík <milan.petrik@protonmail.com>
#
# Web page of the program: <https://gitlab.com/petrikm/teachmedijkstra>
#
# teachmedijkstra is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# teachmedijkstra is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# teachmedijkstra. If not, see <https://www.gnu.org/licenses/>.

"""
The purpose of this code is to show how to create a number of exercises on
Dijkstra's algorithm with randomly generated graphs.
"""

import random
import teachmedijkstra

# Randomly generated exercises will be stored to this list as 2-tuples (pairs)
# where the first value is an instance of `teachmedijkstra.Dijkstra` while the
# second value is a string containing the name of the starting vertex from
# which the shortest paths are to be found.
exercises = []

counter = 0
while counter < 20:
    graph = teachmedijkstra.UndirectedGraph()

    graph.addVertex("a", (0, 2))
    graph.addVertex("b", (1, 2))
    graph.addVertex("c", (2, 2))
    graph.addVertex("d", (0, 1))
    graph.addVertex("e", (1, 1))
    graph.addVertex("f", (2, 1))
    graph.addVertex("g", (0, 0))
    graph.addVertex("h", (1, 0))
    graph.addVertex("i", (2, 0))

    # adding these edges assures that the graph will be connected
    graph.addEdge("a", "b", random.randint(1, 9))
    graph.addEdge("b", "c", random.randint(1, 9))
    graph.addEdge("d", "e", random.randint(1, 9))
    graph.addEdge("e", "f", random.randint(1, 9))
    graph.addEdge("g", "h", random.randint(1, 9))
    graph.addEdge("h", "i", random.randint(1, 9))
    graph.addEdge("b", "e", random.randint(1, 9))
    graph.addEdge("e", "h", random.randint(1, 9))

    # randomly added edges
    if random.randint(1, 10) > 1:
        graph.addEdge("a", "d", random.randint(1, 9))
    if random.randint(1, 10) > 1:
        graph.addEdge("c", "f", random.randint(1, 9))
    if random.randint(1, 10) > 1:
        graph.addEdge("d", "g", random.randint(1, 9))
    if random.randint(1, 10) > 1:
        graph.addEdge("f", "i", random.randint(1, 9))
    if random.randint(1, 10) > 1:
        if random.randint(1, 2) == 1:
            graph.addEdge("a", "e", random.randint(1, 9))
        else:
            graph.addEdge("b", "d", random.randint(1, 9))
    if random.randint(1, 10) > 1:
        if random.randint(1, 2) == 1:
            graph.addEdge("b", "f", random.randint(1, 9))
        else:
            graph.addEdge("c", "e", random.randint(1, 9))
    if random.randint(1, 10) > 1:
        if random.randint(1, 2) == 1:
            graph.addEdge("d", "h", random.randint(1, 9))
        else:
            graph.addEdge("e", "g", random.randint(1, 9))
    if random.randint(1, 10) > 1:
        if random.randint(1, 2) == 1:
            graph.addEdge("e", "i", random.randint(1, 9))
        else:
            graph.addEdge("f", "h", random.randint(1, 9))
    if random.randint(1, 10) > 1:
        graph.addEdge("a", "g", random.randint(1, 9), "bend right=60")
    if random.randint(1, 10) > 1:
        graph.addEdge("c", "i", random.randint(1, 9), "bend left=60")

    startingVertex = chr(random.randint(1, 9) + 96)

    dijkstra = teachmedijkstra.Dijkstra(graph, startingVertex)
    dijkstra.run()

    # Only "appropriately challenging" exercises are added, that is, only those
    # where the number of the cases, when the distance of a vertex is replaces
    # by a shorter one, falls into a given interval.
    if 3 <= dijkstra.numOverwrites <= (dijkstra.maxOverwrites - 2):
        exercises.append((dijkstra, startingVertex))
        counter += 1

# Each exercise is exported to a LaTeX file as the triplet: picture of the
# graph, table with the algorithm steps, picture of the shortest path tree.
path = "out_random.tex"
print("Writing file", path)
with open(path, "w") as out:
    out.write(teachmedijkstra.getLaTeXHead())
    for i in range(len(exercises)):
        (dijkstra, startingVertex) = exercises[i]
        out.write(r"\noindent\textbf{Exercise:} Find the shortest paths" + "\n")
        out.write(r"that start by the vertex $" + startingVertex + "$\n") 
        out.write(r"to all the vertices of the following graph:" + "\n")
        out.write(r"\begin{center}" + "\n")
        out.write(dijkstra.graph.exportToTikZ())
        out.write(r"\end{center}" + "\n")
        out.write(r"\noindent\textbf{Solution:}" + "\n")
        out.write(r"The steps of the Dijkstra's algorithm:" + "\n")
        out.write(r"\begin{center}" + "\n")
        out.write(dijkstra.exportTableToLaTeX())
        out.write(r"\end{center}" + "\n")
        out.write(r"The shortest path tree:" + "\n")
        out.write(r"\begin{center}" + "\n")
        out.write(dijkstra.exportShortestPathTreeToTikZ())
        out.write(r"\end{center}" + "\n")
        out.write(r"\newpage" + "\n")
    out.write(teachmedijkstra.getLaTeXFoot())

