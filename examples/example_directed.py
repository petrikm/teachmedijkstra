# -*- coding: UTF-8 -*-

# This file is a part of teachmedijkstra which is a Python3 package designed
# for educational purposes to demonstrate the Dijkstra's algorithm.
#
# Copyright (C) 2021 Milan Petrík <milan.petrik@protonmail.com>
#
# Web page of the program: <https://gitlab.com/petrikm/teachmedijkstra>
#
# teachmedijkstra is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# teachmedijkstra is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# teachmedijkstra. If not, see <https://www.gnu.org/licenses/>.

"""
This example code:
  * defines a directed weighted graph,
  * performs the Dijkstra's algorithm to find the shortest paths starting from
    the vertex 'a',
  * exports the result to a LaTeX file "out_directed.tex".
"""

import teachmedijkstra

graph = teachmedijkstra.DirectedGraph()
graph.addVertex("a", (0,2))
graph.addVertex("b", (1,2))
graph.addVertex("c", (2,2))
graph.addVertex("d", (0,1))
graph.addVertex("e", (1,1))
graph.addVertex("f", (2,1))
graph.addVertex("g", (0,0))
graph.addVertex("h", (1,0))
graph.addVertex("i", (2,0))
graph.addEdge("a", "b", 9)
graph.addEdge("b", "c", 3)
graph.addEdge("c", "f", 4)
graph.addEdge("e", "f", 3)
graph.addEdge("e", "d", 6)
graph.addEdge("d", "g", 1)
graph.addEdge("g", "h", 3)
graph.addEdge("h", "i", 8)
graph.addEdge("a", "d", 1)
graph.addEdge("e", "b", 3)
graph.addEdge("e", "h", 9)
graph.addEdge("f", "i", 6)
graph.addEdge("a", "e", 4)
graph.addEdge("c", "e", 5)
graph.addEdge("g", "e", 1)
graph.addEdge("i", "e", 4)
#graph.addEdge("a", "c", 2, "bend left=60")
graph.addEdge("c", "i", 6, "bend left=60")
graph.addEdge("a", "g", 1, "bend right=60, swap")
#graph.addEdge("g", "i", 5, "bend right=60, swap")

dijkstra = teachmedijkstra.Dijkstra(graph, "a")
dijkstra.run()

path = "out_directed.tex"
print("Writing file", path)
dijkstra.saveToLaTeXFile(path)

