
Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2021-03-09
  * The program is able to:
      - define a weighted graph, directed or undirected,
      - export such a graph as a TikZ image,
      - perform the Dijkstra's algorithm on this graph for a given starting
        vertex,
      - export the steps of the Dijkstra's algorithm in a form of a LaTeX
        table,
      - export the shortest path tree (shortest path covering)

[0.1.0]: https://gitlab.com/petrikm/teachmedijkstra/-/tags/0.1.0

